from django.conf.urls import url
from . import views


app_name = 'MDC_Website'
urlpatterns = [
        url(r'^$|^/$', views.ProductListView.as_view(), name = 'show_products'),
        #url(r'^(?P<category_slug>[-\w]+)/$', views.showProducts, name='show_products_with_category'),
        url(r'register$', views.register, name = 'register'),
        url(r'login$', views.login, name = 'login'),
        url(r'viewProduct/(?P<slug>[-\w]+)', views.viewProduct, name='view_product'),
        url(r'logout$', views.logout, name = 'logout'),
        url(r'account', views.account, name = 'account'),
        url(r'list$', views.product_list, name='product_list'),
        ]
