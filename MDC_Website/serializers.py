from rest_framework import serializers
from .models import Product


class ProductSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only = True)
    product_name = serializers.CharField(max_length = 60)
    product_description = serializers.CharField(max_length = 500)
    product_image = serializers.ImageField(max_length = 25)
    product_price = serializers.DecimalField(max_digits = 5, decimal_places = 2)
    product_slug = serializers.SlugField(max_length = 30)
    product_created_date = serializers.DateField()
    product_last_updates = serializers.DateTimeField()
    product_expire_date = serializers.DateField()
    product_available_items = serializers.IntegerField(max_value = None, min_value = None)
    product_availability = serializers.BooleanField()
    
    def create(self, validated_data):
        return Product.objects.create(**validated_data)

    def update(self, instance, validated_date):
        instance.product_name = validated_data.get('product_name', instance.product_name)
        instance.product_description = validated_data.get('product_description', instance.product_description)
        instance.product_image = instance.get('product_image', instance.product_image)
        instance.product_price = instance.get('product_price', instance.product_price)
        instance.product_slug = instance.get('product_slug', instance.product_slug)
        instance.product_date = instance.get('product_created_date', instance.product_created_date)
        instance.product_updates = instance.get('product_last_updates', instance.product_last_updates)
        instance.product_expire_date = instance.get('product_expire_date', instance.product_expire_date)
        instance.product_available_items = instance.get('product_available_items',
                instance.product_available_items)
        instance.product_availability = instance.get('product_availability', instance.product_availability)
        instance.save()
        return instance
    #Revisit datetime module
