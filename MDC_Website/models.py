from django.db import models
from django.urls import reverse
# Create your models here.


class AvailableProducts(models.Manager):
    def get_queryset(self):
        return super(AvailableProducts,
                self).get_queryset().filter(product_availability = True)

class Category(models.Model):
    category_name = models.CharField(max_length = 50,
            unique = True,
            db_index = True)
    category_description = models.TextField(max_length = 500,
            db_index = True)
    category_slug = models.SlugField(max_length = 30,
            db_index = True)

    def __str__(self):
        return self.category_name
    
    class Meta:
        ordering = ('category_name',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'

class Product(models.Model):
    product_name = models.CharField(max_length = 60,
            unique = True,
            db_index = True)
    product_description = models.TextField(max_length = 500,
            db_index = True)
    product_image = models.ImageField(upload_to = 'media', blank = True)
    product_price = models.DecimalField(max_digits = 4, decimal_places = 2)
    product_slug = models.SlugField(max_length = 30)
    product_created_date = models.DateField()
    product_last_updates = models.DateTimeField(auto_now = True)
    product_expire_date = models.DateField(auto_now = False, auto_now_add = False)
    product_available_items = models.PositiveIntegerField()
    product_availability = models.BooleanField(default = False)
    product_category = models.ForeignKey(Category, on_delete = models.CASCADE)
    
    objects = models.Manager()
    available = AvailableProducts()

    def get_absolute_url(self):
        return reverse('MDC_Website:view_product', args = [self.product_slug])

    def __str__(self):
        return self.product_name

    class Meta:
        ordering = ('product_name',)
        verbose_name = 'product'
        verbose_name_plural = 'products'

