from django.contrib import admin
from .models import Category, Product
# Register your models here.


class ProductAdmin(admin.ModelAdmin):
    list_display = ('product_name',
            'product_description',
            'product_created_date',
            'product_expire_date',
            'product_category',
            )
    list_filter = ('product_category', 'product_price')
    search_fields = ('product_name',)
    prepopulated_fields = {'product_slug': ('product_name',)}

admin.site.register(Category)
admin.site.register(Product, ProductAdmin)
