from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView
from .forms import Register, Login, Products
from django.views.decorators.http import require_http_methods
from .models import Category, Product
from cart.forms import CartAddProductForm
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import status
from .serializers import ProductSerializer

# Create your views here
#I won't use class until I know how to use tags within it
class ProductListView(ListView):
    queryset = Product.available.all()
    context_object_name = 'products'
    paginate_by = 9
    template_name = 'MDC_Website/product/MDC_Products.html'

#Use this tempo
def showProducts(request, category_slug = None):
    category = None  
    categories = Category.objects.all()
    products = Product.objects.filter(product_availability = True)
    if category_slug:
        category = get_object_or_404(Category, slug = category_slug)
        products = products.filter(product_category = category)
    return render(request, 'MDC_Website/product/MDC_Products.html', {'products': products,
        'categories': categories,
        'category': category})

#Show product details
def viewProduct(request, slug):
    product = get_object_or_404(Product,
            product_slug = slug,
            product_availability = True)
    cart_product_form = CartAddProductForm()
    return render(request, 'MDC_Website/product/MDC_viewProduct.html', {'product':product,
        'cart_product_form': cart_product_form})


def register(request):
    if request.method == 'POST':
        form = Register(request.POST)
        if form.is_valid():
            user_name = form.cleaned_data['user_name']
            user_password = form.cleaned_data['user_password']
            user_address = form.cleaned_data['user_address']
            user_age = form.cleaned_data['user_age']
            user_disease = form.cleaned_data['user_disease']
            user_email = form.cleaned_data['user_email']
            user_description = form.cleaned_data['user_description']
            user_chronic_disease = form.cleaned_data['user_chronic_disease']
            user_categoury = form.cleaned_data['user_categoury']
            mdc_email = "servertest587@gmail.com"
            user_add = ("INSERT INTO user (u_name, u_password,u_address, u_age, u_disease, u_email,"
                    "u_description, u_chronic_disease, u_categoury) VALUES"
                    " (%s, %s, %s, %s, %s, %s, %s, %s, %s)")
            user_data = (user_name, user_password,user_address, user_age, user_disease,
                    user_email, user_description, user_chronic_disease, 
                    user_categoury)
            cursor = db.databaseConnect(user_add,True, user_data, statement = 'edit')
            msg = "Thx for registration"
            email = smtplib.SMTP(host = 'smtp.gmail.com', port = 587)
            email.starttls()
            email.login(user = 'servertest587', password = 'GMAILTEST587')
            email.ehlo()
            email.sendmail(mdc_email, user_email, msg)
            email.quit()
            welcome_message = "Thx for registration"
        return render(request, 'MDC_Website/MDC_Register.html',
                    {'form':form, 'welcome_message':welcome_message})
    else:
        form = Register()
    return render(request, 'MDC_Website/MDC_Register.html', {'form':form}) 
    
def login(request):
    user_id=''
    user_password = ''
    user_name = ''
    if request.method == 'POST':
        login_form = Login(request.POST)
        if login_form.is_valid():
            user_email = login_form.cleaned_data['user_name']
            user_password = login_form.cleaned_data['user_password']
            login_query = ("SELECT u_name, u_id FROM user WHERE u_email=%s AND u_password=%s")
            email_password = (user_email, user_password)
            data = db.databaseConnect(login_query, True, email_password)
            #check for user email and password
            i = 0
            for u_name, u_id in data:
                i = i + 1
                user_id = u_id
                user_name = u_name
            if i == 1:
                request.session['user_id'] = user_id
                request.session['user_name'] = user_name
                request.session.modified = True
                message = ('Welcome back {}'.format(request.session['user_name']))
                return render(request, 'MDC_Website/MDC_Home.html', {'message':message})
            elif i > 1:
                message = "Duplicate user name and password"
            else:
                message = "Wrong user name or password"
            return render(request, 'MDC_Website/MDC_Login.html', {'login_form':login_form, 'message':message})
    else:
        login_form = Login()
    return render(request, 'MDC_Website/MDC_Login.html',{'login_form':login_form})

def account(request):
    query = "SELECT order_id, p_id FROM user_product WHERE u_id = {}".format(request.session['user_id']) 
    message = "Hello:{} ".format(request.session['user_name'])
    data = db.databaseConnect(query)
    webpage_data = {
            'message':message,
            'products':data}
    return render(request, 'MDC_Website/MDC_Account.html', webpage_data)

def logout(request):
    try:
        del request.session['user_id']
        request.session.flush()
    except KeyError:
        pass
    return render(request, 'MDC_Website/MDC_Home.html')

#REST API
class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)



#To list all products or insert new one
@csrf_exempt
def product_list(request):
    if request.method == 'GET':
        products = Product.objects.all()
        products_serializer = ProductSerializer(products, many = True)
        return JSONResponse(products_serializer.data)
    elif request.method == 'POST':
        new_product = JSONParser().parse(request)
        product_serializer = ProductSerializer(data = new_product)
        if product_serializer.is_valid():
            product_serializer.save()
            return JSONResponse(product_serializer.data, status = status.HTTP_201_CREATED)
    return JSONResponse(products_serializer.errors, status = status.HTTP_400_BAD_REQUEST)



